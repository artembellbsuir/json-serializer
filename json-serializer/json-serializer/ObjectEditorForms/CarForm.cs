﻿using json_serializer.Entities;
using System;
using System.Windows.Forms;

namespace json_serializer.ObjectEditorForms
{
    public partial class CarForm : Form
    {
        private Car Car;
        private Action<WheelyVehicle> SaveVehicle;
        public CarForm(Car Vehicle, Action<WheelyVehicle> SaveVehicle)
        {
            InitializeComponent();

            this.SaveVehicle = SaveVehicle;
            Car = new Car(Vehicle);

            tbVIN.Text = Vehicle.VehicleIdentificationNumber;
            tbManufacturer.Text = Vehicle.Manufacturer;
            tbOwner.Text = Vehicle.Owner;
            tbPrice.Text = Vehicle.Price.ToString();
            tbWheels.Text = Vehicle.Wheels.ToString();
            tbSeats.Text = Vehicle.Seats.ToString();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            Car.Owner = tbOwner.Text;
            Car.Manufacturer = tbManufacturer.Text;
            Car.Wheels = int.Parse(tbWheels.Text);
            Car.Price = int.Parse(tbPrice.Text);
            Car.Seats = int.Parse(tbSeats.Text);

            SaveVehicle(Car);
            Close();
        }

        private void tbSeats_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblSeats_Click(object sender, EventArgs e)
        {

        }

        private void lblWheels_Click(object sender, EventArgs e)
        {

        }

        private void tbWheels_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblPrice_Click(object sender, EventArgs e)
        {

        }

        private void tbPrice_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblManufacturer_Click(object sender, EventArgs e)
        {

        }

        private void tbManufacturer_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblOwner_Click(object sender, EventArgs e)
        {

        }

        private void tbOwner_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblVIN_Click(object sender, EventArgs e)
        {

        }

        private void tbVIN_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

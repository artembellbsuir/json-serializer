﻿namespace json_serializer.ObjectEditorForms
{
    partial class MotorcycleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSeats = new System.Windows.Forms.Label();
            this.tbMileage = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblWheels = new System.Windows.Forms.Label();
            this.tbWheels = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.lblManufacturer = new System.Windows.Forms.Label();
            this.tbManufacturer = new System.Windows.Forms.TextBox();
            this.lblOwner = new System.Windows.Forms.Label();
            this.tbOwner = new System.Windows.Forms.TextBox();
            this.lblVIN = new System.Windows.Forms.Label();
            this.tbVIN = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSeats
            // 
            this.lblSeats.AutoSize = true;
            this.lblSeats.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblSeats.Location = new System.Drawing.Point(20, 339);
            this.lblSeats.Name = "lblSeats";
            this.lblSeats.Size = new System.Drawing.Size(57, 17);
            this.lblSeats.TabIndex = 25;
            this.lblSeats.Text = "Mileage";
            // 
            // tbMileage
            // 
            this.tbMileage.Location = new System.Drawing.Point(23, 359);
            this.tbMileage.Name = "tbMileage";
            this.tbMileage.Size = new System.Drawing.Size(277, 22);
            this.tbMileage.TabIndex = 22;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(23, 404);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(277, 23);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // lblWheels
            // 
            this.lblWheels.AutoSize = true;
            this.lblWheels.Location = new System.Drawing.Point(20, 272);
            this.lblWheels.Name = "lblWheels";
            this.lblWheels.Size = new System.Drawing.Size(55, 17);
            this.lblWheels.TabIndex = 23;
            this.lblWheels.Text = "Wheels";
            // 
            // tbWheels
            // 
            this.tbWheels.Location = new System.Drawing.Point(23, 292);
            this.tbWheels.Name = "tbWheels";
            this.tbWheels.Size = new System.Drawing.Size(277, 22);
            this.tbWheels.TabIndex = 21;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(20, 203);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(40, 17);
            this.lblPrice.TabIndex = 20;
            this.lblPrice.Text = "Price";
            // 
            // tbPrice
            // 
            this.tbPrice.Location = new System.Drawing.Point(23, 223);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(277, 22);
            this.tbPrice.TabIndex = 19;
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.AutoSize = true;
            this.lblManufacturer.Location = new System.Drawing.Point(20, 136);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(92, 17);
            this.lblManufacturer.TabIndex = 18;
            this.lblManufacturer.Text = "Manufacturer";
            // 
            // tbManufacturer
            // 
            this.tbManufacturer.Location = new System.Drawing.Point(23, 156);
            this.tbManufacturer.Name = "tbManufacturer";
            this.tbManufacturer.Size = new System.Drawing.Size(277, 22);
            this.tbManufacturer.TabIndex = 17;
            // 
            // lblOwner
            // 
            this.lblOwner.AutoSize = true;
            this.lblOwner.Location = new System.Drawing.Point(20, 73);
            this.lblOwner.Name = "lblOwner";
            this.lblOwner.Size = new System.Drawing.Size(49, 17);
            this.lblOwner.TabIndex = 16;
            this.lblOwner.Text = "Owner";
            // 
            // tbOwner
            // 
            this.tbOwner.Location = new System.Drawing.Point(23, 93);
            this.tbOwner.Name = "tbOwner";
            this.tbOwner.Size = new System.Drawing.Size(277, 22);
            this.tbOwner.TabIndex = 15;
            // 
            // lblVIN
            // 
            this.lblVIN.AutoSize = true;
            this.lblVIN.Location = new System.Drawing.Point(20, 16);
            this.lblVIN.Name = "lblVIN";
            this.lblVIN.Size = new System.Drawing.Size(191, 17);
            this.lblVIN.TabIndex = 14;
            this.lblVIN.Text = "Vehicle Identification Number";
            // 
            // tbVIN
            // 
            this.tbVIN.Location = new System.Drawing.Point(23, 36);
            this.tbVIN.Name = "tbVIN";
            this.tbVIN.ReadOnly = true;
            this.tbVIN.Size = new System.Drawing.Size(277, 22);
            this.tbVIN.TabIndex = 13;
            // 
            // MotorcycleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 453);
            this.Controls.Add(this.lblSeats);
            this.Controls.Add(this.tbMileage);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblWheels);
            this.Controls.Add(this.tbWheels);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.tbPrice);
            this.Controls.Add(this.lblManufacturer);
            this.Controls.Add(this.tbManufacturer);
            this.Controls.Add(this.lblOwner);
            this.Controls.Add(this.tbOwner);
            this.Controls.Add(this.lblVIN);
            this.Controls.Add(this.tbVIN);
            this.Name = "MotorcycleForm";
            this.Text = "MotorcycleForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSeats;
        private System.Windows.Forms.TextBox tbMileage;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblWheels;
        private System.Windows.Forms.TextBox tbWheels;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox tbPrice;
        private System.Windows.Forms.Label lblManufacturer;
        private System.Windows.Forms.TextBox tbManufacturer;
        private System.Windows.Forms.Label lblOwner;
        private System.Windows.Forms.TextBox tbOwner;
        private System.Windows.Forms.Label lblVIN;
        private System.Windows.Forms.TextBox tbVIN;
    }
}
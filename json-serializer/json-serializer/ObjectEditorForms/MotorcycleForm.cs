﻿using json_serializer.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace json_serializer.ObjectEditorForms
{
    public partial class MotorcycleForm : Form
    {
        private Motorcycle Vehicle;
        private Action<WheelyVehicle> SaveVehicle;
        public MotorcycleForm(Motorcycle Vehicle, Action<WheelyVehicle> SaveVehicle)
        {
            InitializeComponent();

            this.SaveVehicle = SaveVehicle;
            this.Vehicle = new Motorcycle(Vehicle);

            tbVIN.Text = Vehicle.VehicleIdentificationNumber;
            tbManufacturer.Text = Vehicle.Manufacturer;
            tbOwner.Text = Vehicle.Owner;
            tbPrice.Text = Vehicle.Price.ToString();
            tbWheels.Text = Vehicle.Wheels.ToString();
            tbMileage.Text = Vehicle.Mileage.ToString();
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            Vehicle.Owner = tbOwner.Text;
            Vehicle.Manufacturer = tbManufacturer.Text;
            Vehicle.Wheels = int.Parse(tbWheels.Text);
            Vehicle.Price = int.Parse(tbPrice.Text);
            Vehicle.Mileage = int.Parse(tbMileage.Text);

            SaveVehicle(Vehicle);
            Close();
        }
    }
}

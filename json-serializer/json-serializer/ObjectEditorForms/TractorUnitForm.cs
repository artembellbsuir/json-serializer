﻿using json_serializer.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace json_serializer.ObjectEditorForms
{
    public partial class TractorUnitForm : Form
    {
        private TractorUnit Vehicle;
        private Action<WheelyVehicle> SaveVehicle;
        public TractorUnitForm(TractorUnit Vehicle, Action<WheelyVehicle> SaveVehicle)
        {
            InitializeComponent();

            this.SaveVehicle = SaveVehicle;
            this.Vehicle = new TractorUnit(Vehicle);

            tbVIN.Text = Vehicle.VehicleIdentificationNumber;
            tbManufacturer.Text = Vehicle.Manufacturer;
            tbOwner.Text = Vehicle.Owner;
            tbPrice.Text = Vehicle.Price.ToString();
            tbWheels.Text = Vehicle.Wheels.ToString();
            tbGears.Text = Vehicle.Gears.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Vehicle.Owner = tbOwner.Text;
            Vehicle.Manufacturer = tbManufacturer.Text;
            Vehicle.Wheels = int.Parse(tbWheels.Text);
            Vehicle.Price = int.Parse(tbPrice.Text);
            Vehicle.Gears = int.Parse(tbGears.Text);

            SaveVehicle(Vehicle);
            Close();
        }
    }
}

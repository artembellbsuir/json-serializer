﻿namespace json_serializer.ObjectEditorForms
{
    partial class CarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbVIN = new System.Windows.Forms.TextBox();
            this.lblVIN = new System.Windows.Forms.Label();
            this.lblOwner = new System.Windows.Forms.Label();
            this.tbOwner = new System.Windows.Forms.TextBox();
            this.lblManufacturer = new System.Windows.Forms.Label();
            this.tbManufacturer = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.lblWheels = new System.Windows.Forms.Label();
            this.tbWheels = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblSeats = new System.Windows.Forms.Label();
            this.tbSeats = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbVIN
            // 
            this.tbVIN.Location = new System.Drawing.Point(25, 43);
            this.tbVIN.Name = "tbVIN";
            this.tbVIN.ReadOnly = true;
            this.tbVIN.Size = new System.Drawing.Size(277, 22);
            this.tbVIN.TabIndex = 0;
            this.tbVIN.TextChanged += new System.EventHandler(this.tbVIN_TextChanged);
            // 
            // lblVIN
            // 
            this.lblVIN.AutoSize = true;
            this.lblVIN.Location = new System.Drawing.Point(22, 23);
            this.lblVIN.Name = "lblVIN";
            this.lblVIN.Size = new System.Drawing.Size(191, 17);
            this.lblVIN.TabIndex = 1;
            this.lblVIN.Text = "Vehicle Identification Number";
            this.lblVIN.Click += new System.EventHandler(this.lblVIN_Click);
            // 
            // lblOwner
            // 
            this.lblOwner.AutoSize = true;
            this.lblOwner.Location = new System.Drawing.Point(22, 80);
            this.lblOwner.Name = "lblOwner";
            this.lblOwner.Size = new System.Drawing.Size(49, 17);
            this.lblOwner.TabIndex = 3;
            this.lblOwner.Text = "Owner";
            this.lblOwner.Click += new System.EventHandler(this.lblOwner_Click);
            // 
            // tbOwner
            // 
            this.tbOwner.Location = new System.Drawing.Point(25, 100);
            this.tbOwner.Name = "tbOwner";
            this.tbOwner.Size = new System.Drawing.Size(277, 22);
            this.tbOwner.TabIndex = 2;
            this.tbOwner.TextChanged += new System.EventHandler(this.tbOwner_TextChanged);
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.AutoSize = true;
            this.lblManufacturer.Location = new System.Drawing.Point(22, 143);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(92, 17);
            this.lblManufacturer.TabIndex = 5;
            this.lblManufacturer.Text = "Manufacturer";
            this.lblManufacturer.Click += new System.EventHandler(this.lblManufacturer_Click);
            // 
            // tbManufacturer
            // 
            this.tbManufacturer.Location = new System.Drawing.Point(25, 163);
            this.tbManufacturer.Name = "tbManufacturer";
            this.tbManufacturer.Size = new System.Drawing.Size(277, 22);
            this.tbManufacturer.TabIndex = 4;
            this.tbManufacturer.TextChanged += new System.EventHandler(this.tbManufacturer_TextChanged);
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(22, 210);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(40, 17);
            this.lblPrice.TabIndex = 7;
            this.lblPrice.Text = "Price";
            this.lblPrice.Click += new System.EventHandler(this.lblPrice_Click);
            // 
            // tbPrice
            // 
            this.tbPrice.Location = new System.Drawing.Point(25, 230);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(277, 22);
            this.tbPrice.TabIndex = 6;
            this.tbPrice.TextChanged += new System.EventHandler(this.tbPrice_TextChanged);
            // 
            // lblWheels
            // 
            this.lblWheels.AutoSize = true;
            this.lblWheels.Location = new System.Drawing.Point(22, 279);
            this.lblWheels.Name = "lblWheels";
            this.lblWheels.Size = new System.Drawing.Size(55, 17);
            this.lblWheels.TabIndex = 9;
            this.lblWheels.Text = "Wheels";
            this.lblWheels.Click += new System.EventHandler(this.lblWheels_Click);
            // 
            // tbWheels
            // 
            this.tbWheels.Location = new System.Drawing.Point(25, 299);
            this.tbWheels.Name = "tbWheels";
            this.tbWheels.Size = new System.Drawing.Size(277, 22);
            this.tbWheels.TabIndex = 8;
            this.tbWheels.TextChanged += new System.EventHandler(this.tbWheels_TextChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(25, 411);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(277, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // lblSeats
            // 
            this.lblSeats.AutoSize = true;
            this.lblSeats.Location = new System.Drawing.Point(22, 346);
            this.lblSeats.Name = "lblSeats";
            this.lblSeats.Size = new System.Drawing.Size(44, 17);
            this.lblSeats.TabIndex = 12;
            this.lblSeats.Text = "Seats";
            this.lblSeats.Click += new System.EventHandler(this.lblSeats_Click);
            // 
            // tbSeats
            // 
            this.tbSeats.Location = new System.Drawing.Point(25, 366);
            this.tbSeats.Name = "tbSeats";
            this.tbSeats.Size = new System.Drawing.Size(277, 22);
            this.tbSeats.TabIndex = 9;
            this.tbSeats.TextChanged += new System.EventHandler(this.tbSeats_TextChanged);
            // 
            // CarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 458);
            this.Controls.Add(this.lblSeats);
            this.Controls.Add(this.tbSeats);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblWheels);
            this.Controls.Add(this.tbWheels);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.tbPrice);
            this.Controls.Add(this.lblManufacturer);
            this.Controls.Add(this.tbManufacturer);
            this.Controls.Add(this.lblOwner);
            this.Controls.Add(this.tbOwner);
            this.Controls.Add(this.lblVIN);
            this.Controls.Add(this.tbVIN);
            this.Name = "CarForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Car";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbVIN;
        private System.Windows.Forms.Label lblVIN;
        private System.Windows.Forms.Label lblOwner;
        private System.Windows.Forms.TextBox tbOwner;
        private System.Windows.Forms.Label lblManufacturer;
        private System.Windows.Forms.TextBox tbManufacturer;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox tbPrice;
        private System.Windows.Forms.Label lblWheels;
        private System.Windows.Forms.TextBox tbWheels;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSeats;
        private System.Windows.Forms.TextBox tbSeats;
    }
}
﻿using json_serializer.Entities;
using json_serializer.ObjectEditorForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace json_serializer.Handlers
{
    public class CreateHandler
    {
        private readonly Dictionary<string, Action> HandlerOptions;
        public string[] Options
        {
            get { return HandlerOptions.Keys.ToArray(); }
        }
        private readonly Action<WheelyVehicle> SaveNewHandler;
        public CreateHandler(Action<WheelyVehicle> saveNewHandler)
        {
            SaveNewHandler = saveNewHandler;
            HandlerOptions = new Dictionary<string, Action>
            {
                { "Car", CreateCar },
                { "Motorcycle", CreateMotorcycle },
                { "Trailer", CreateTrailer },
                { "Tractor Unit", CreateTractorUnit },
                { "Truck", CreateTruck },
            };
        }

        private void CreateCar()
        {
            new CarForm(new Car(), SaveNewHandler).Show();
        }

        private void CreateMotorcycle()
        {
            new MotorcycleForm(new Motorcycle(), SaveNewHandler).Show();
        }

        private void CreateTrailer()
        {
            new TrailerForm(new Trailer(), SaveNewHandler).Show();
        }

        private void CreateTractorUnit()
        {
            new TractorUnitForm(new TractorUnit(), SaveNewHandler).Show();
        }

        private void CreateTruck()
        {
            new TruckForm(new Truck(), SaveNewHandler).Show();
        }

        public void Perform(string chosen)
        {
            HandlerOptions[chosen]();
        }
    }
}

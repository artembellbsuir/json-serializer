﻿using json_serializer.Entities;
using json_serializer.ObjectEditorForms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace json_serializer.Handlers
{
    public class EditHandler
    {
        private readonly Dictionary<string, Action<WheelyVehicle>> HandlerOptions;
        public string[] Options
        {
            get { return HandlerOptions.Keys.ToArray(); }
        }
        public Action<WheelyVehicle> SaveEdited;
        public EditHandler(Action<WheelyVehicle> saveEdited)
        {
            SaveEdited = saveEdited;
            HandlerOptions = new Dictionary<string, Action<WheelyVehicle>>
            {
                { "Car", EditCar },
                { "Motorcycle", EditMotorcycle },
                { "Trailer", EditTrailer },
                { "Tractor Unit", EditTractorUnit },
                { "Truck", EditTruck },
            };
        }

        private void EditCar(WheelyVehicle Vehicle)
        {
            new CarForm((Car)Vehicle, SaveEdited).Show();
        }

        private void EditMotorcycle(WheelyVehicle Vehicle)
        {
            new MotorcycleForm((Motorcycle)Vehicle, SaveEdited).Show();
        }

        private void EditTrailer(WheelyVehicle Vehicle)
        {
            new TrailerForm((Trailer)Vehicle, SaveEdited).Show();
        }

        private void EditTractorUnit(WheelyVehicle Vehicle)
        {
            new TractorUnitForm((TractorUnit)Vehicle, SaveEdited).Show();
        }
        private void EditTruck(WheelyVehicle Vehicle)
        {
            new TruckForm((Truck)Vehicle, SaveEdited).Show();
        }

        public void Perform(object selectedObject)
        {
            string Name = ((WheelyVehicle)selectedObject).Name;
            HandlerOptions[Name]((WheelyVehicle)selectedObject);
        }
    }
}

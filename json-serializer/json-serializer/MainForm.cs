﻿using json_serializer.DataManager;
using json_serializer.Entities;
using json_serializer.Handlers;
using json_serializer.ObjectEditorForms;
using json_serializer.Serializer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace json_serializer
{
    public partial class MainForm : Form
    {
        private ObjectManager<WheelyVehicle> dataManager;
        private CustomJsonSerializer<List<WheelyVehicle>> jsonSerializer;


        private CreateHandler CreateHandler;
        private EditHandler EditHandler;
        public MainForm()
        {
            InitializeComponent();
           
            EditHandler = new EditHandler(OnEditSaveVehicle);
            CreateHandler = new CreateHandler(OnCreateNewVehicle);

            //CreateHandler = new CreateHandler(OnSaveNewVehicle);
            cbCreate.Items.AddRange(CreateHandler.Options);

            dataManager = new ObjectManager<WheelyVehicle>();
            jsonSerializer = new CustomJsonSerializer<List<WheelyVehicle>>();



            WheelyVehicle motorcycle1 = new Motorcycle(170000, 20000, "Ducati");
            WheelyVehicle motorcycle2 = new Motorcycle(90000, 19000, "Suzuki");

            WheelyVehicle tractorUnit = new TractorUnit(14, 50000, "Peterbit");
            WheelyVehicle trailer = new Trailer(800, 25000, "HiRoads");
            WheelyVehicle truck = new Truck(8, 120000, "Bybucks");

            WheelyVehicle car1 = new Car(4, 27000, "Mitsubishi");
            WheelyVehicle car2 = new Car(4, 29000, "Subaru");
            WheelyVehicle car3 = new Car(5, 35000, "Kia");

            dataManager.Add(
                motorcycle1, motorcycle2,
                tractorUnit, trailer, truck,
                car1, car2, car3);

            UpdateOptions();
        }

        public void OnEditSaveVehicle(WheelyVehicle editedVehicle)
        {
            int foundIndex = dataManager.GetAll().FindIndex(v =>
            {
                return v.VehicleIdentificationNumber ==
                    editedVehicle.VehicleIdentificationNumber;
            });

            dataManager.Remove(foundIndex);
            dataManager.Add(editedVehicle);
            UpdateOptions();
        }

        public void OnCreateNewVehicle(WheelyVehicle newVehicle)
        {
            dataManager.Add(newVehicle);
            UpdateOptions();
        }

        public void OnDeleteVehicle(WheelyVehicle deleteVehicle)
        {
            int foundIndex = dataManager.GetAll().FindIndex(v =>
            {
                return v.VehicleIdentificationNumber ==
                    deleteVehicle.VehicleIdentificationNumber;
            });

            dataManager.Remove(foundIndex);
            UpdateOptions();
        }

        public void OnSaveNewVehicle(WheelyVehicle editedVehicle)
        {
            dataManager.Add(editedVehicle);
            UpdateOptions();
        }

        public void UpdateOptions()
        {
            cbEdit.Items.Clear();
            cbEdit.Items.AddRange(dataManager.GetAll().ToArray());
            cbDelete.Items.Clear();
            cbDelete.Items.AddRange(dataManager.GetAll().ToArray());
        }

        private void CbObjects_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (cb.SelectedIndex != -1)
            {
                WheelyVehicle chosen = (WheelyVehicle)cb.SelectedItem;
                EditHandler.Perform(chosen);
            }
        }

        private void CbDelete_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (cb.SelectedIndex != -1)
            {
                WheelyVehicle chosen = (WheelyVehicle)cb.SelectedItem;
                dataManager.Remove(chosen);
                UpdateOptions();
            }
        }

        private void CbCreate_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (cb.SelectedIndex != -1)
            {
                string chosen = (string)cb.SelectedItem;
                CreateHandler.Perform(chosen);
            }
        }

        private void BtnSerialize_Click(object sender, EventArgs e)
        {
            string filePath = "vehicles.json";
            jsonSerializer.Serialize(dataManager.GetAll(), filePath);
        }

        private void BtnDeserialize_Click(object sender, EventArgs e)
        {
            string filePath = "vehicles.json";
            List<WheelyVehicle> objects = jsonSerializer.Deserialize(filePath);

            dataManager.RemoveAll();
            dataManager.Add(objects.ToArray());
            UpdateOptions();
        }
    }
}

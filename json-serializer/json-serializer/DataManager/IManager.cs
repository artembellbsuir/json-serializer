﻿using System.Collections.Generic;

namespace json_serializer.DataManager
{
    public interface IManager<T>
    {
        void Add(T obj);
        void Add(params T[] objects);
        List<T> GetAll();
        void Remove(int position);
        void Remove(T item);
        void RemoveAll();
    }
}

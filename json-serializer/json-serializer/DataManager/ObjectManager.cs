﻿using System.Collections.Generic;

namespace json_serializer.DataManager
{
    public class ObjectManager<T> : IManager<T>
    {
        private readonly List<T> Objects;

        public ObjectManager()
        {
            Objects = new List<T>();
        }

        public void Add(T obj)
        {
            Objects.Add(obj);
        }

        public void Add(params T[] objects)
        {
            foreach (T obj in objects)
            {
                this.Objects.Add(obj);
            }
        }

        public List<T> GetAll()
        {
            return Objects;
        }
        public void Remove(int position)
        {
            Objects.RemoveAt(position);
        }

        public void Remove(T item)
        {
            Objects.Remove(item);
        }

        public void RemoveAll()
        {
            Objects.Clear();
        }
    }
}

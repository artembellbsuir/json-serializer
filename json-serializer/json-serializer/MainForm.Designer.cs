﻿namespace json_serializer
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbEdit = new System.Windows.Forms.ComboBox();
            this.gbEdit = new System.Windows.Forms.GroupBox();
            this.gbDelete = new System.Windows.Forms.GroupBox();
            this.cbDelete = new System.Windows.Forms.ComboBox();
            this.gbSerialization = new System.Windows.Forms.GroupBox();
            this.btnDeserialize = new System.Windows.Forms.Button();
            this.btnSerialize = new System.Windows.Forms.Button();
            this.gbCreate = new System.Windows.Forms.GroupBox();
            this.cbCreate = new System.Windows.Forms.ComboBox();
            this.gbEdit.SuspendLayout();
            this.gbDelete.SuspendLayout();
            this.gbSerialization.SuspendLayout();
            this.gbCreate.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbEdit
            // 
            this.cbEdit.FormattingEnabled = true;
            this.cbEdit.Location = new System.Drawing.Point(6, 31);
            this.cbEdit.Name = "cbEdit";
            this.cbEdit.Size = new System.Drawing.Size(479, 24);
            this.cbEdit.TabIndex = 0;
            this.cbEdit.SelectedValueChanged += new System.EventHandler(this.CbObjects_SelectedValueChanged);
            // 
            // gbEdit
            // 
            this.gbEdit.Controls.Add(this.cbEdit);
            this.gbEdit.Location = new System.Drawing.Point(12, 12);
            this.gbEdit.Name = "gbEdit";
            this.gbEdit.Size = new System.Drawing.Size(494, 66);
            this.gbEdit.TabIndex = 1;
            this.gbEdit.TabStop = false;
            this.gbEdit.Text = "Edit object";
            // 
            // gbDelete
            // 
            this.gbDelete.Controls.Add(this.cbDelete);
            this.gbDelete.Location = new System.Drawing.Point(12, 94);
            this.gbDelete.Name = "gbDelete";
            this.gbDelete.Size = new System.Drawing.Size(494, 66);
            this.gbDelete.TabIndex = 2;
            this.gbDelete.TabStop = false;
            this.gbDelete.Text = "Delete object";
            // 
            // cbDelete
            // 
            this.cbDelete.FormattingEnabled = true;
            this.cbDelete.Location = new System.Drawing.Point(6, 31);
            this.cbDelete.Name = "cbDelete";
            this.cbDelete.Size = new System.Drawing.Size(479, 24);
            this.cbDelete.TabIndex = 0;
            this.cbDelete.SelectedValueChanged += new System.EventHandler(this.CbDelete_SelectedValueChanged);
            // 
            // gbSerialization
            // 
            this.gbSerialization.Controls.Add(this.btnDeserialize);
            this.gbSerialization.Controls.Add(this.btnSerialize);
            this.gbSerialization.Location = new System.Drawing.Point(12, 261);
            this.gbSerialization.Name = "gbSerialization";
            this.gbSerialization.Size = new System.Drawing.Size(494, 66);
            this.gbSerialization.TabIndex = 3;
            this.gbSerialization.TabStop = false;
            this.gbSerialization.Text = "Serialization";
            // 
            // btnDeserialize
            // 
            this.btnDeserialize.Location = new System.Drawing.Point(255, 34);
            this.btnDeserialize.Name = "btnDeserialize";
            this.btnDeserialize.Size = new System.Drawing.Size(230, 23);
            this.btnDeserialize.TabIndex = 1;
            this.btnDeserialize.Text = "Deserialize from JSON";
            this.btnDeserialize.UseVisualStyleBackColor = true;
            this.btnDeserialize.Click += new System.EventHandler(this.BtnDeserialize_Click);
            // 
            // btnSerialize
            // 
            this.btnSerialize.Location = new System.Drawing.Point(6, 34);
            this.btnSerialize.Name = "btnSerialize";
            this.btnSerialize.Size = new System.Drawing.Size(231, 23);
            this.btnSerialize.TabIndex = 0;
            this.btnSerialize.Text = "Serialize to JSON";
            this.btnSerialize.UseVisualStyleBackColor = true;
            this.btnSerialize.Click += new System.EventHandler(this.BtnSerialize_Click);
            // 
            // gbCreate
            // 
            this.gbCreate.Controls.Add(this.cbCreate);
            this.gbCreate.Location = new System.Drawing.Point(12, 175);
            this.gbCreate.Name = "gbCreate";
            this.gbCreate.Size = new System.Drawing.Size(494, 66);
            this.gbCreate.TabIndex = 4;
            this.gbCreate.TabStop = false;
            this.gbCreate.Text = "Create object";
            // 
            // cbCreate
            // 
            this.cbCreate.FormattingEnabled = true;
            this.cbCreate.Location = new System.Drawing.Point(6, 31);
            this.cbCreate.Name = "cbCreate";
            this.cbCreate.Size = new System.Drawing.Size(479, 24);
            this.cbCreate.TabIndex = 0;
            this.cbCreate.SelectedValueChanged += new System.EventHandler(this.CbCreate_SelectedValueChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 339);
            this.Controls.Add(this.gbCreate);
            this.Controls.Add(this.gbSerialization);
            this.Controls.Add(this.gbDelete);
            this.Controls.Add(this.gbEdit);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.gbEdit.ResumeLayout(false);
            this.gbDelete.ResumeLayout(false);
            this.gbSerialization.ResumeLayout(false);
            this.gbCreate.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbEdit;
        private System.Windows.Forms.GroupBox gbEdit;
        private System.Windows.Forms.GroupBox gbDelete;
        private System.Windows.Forms.ComboBox cbDelete;
        private System.Windows.Forms.GroupBox gbSerialization;
        private System.Windows.Forms.Button btnDeserialize;
        private System.Windows.Forms.Button btnSerialize;
        private System.Windows.Forms.GroupBox gbCreate;
        private System.Windows.Forms.ComboBox cbCreate;
    }
}


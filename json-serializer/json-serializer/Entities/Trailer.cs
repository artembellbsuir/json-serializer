﻿namespace json_serializer.Entities
{
    public class Trailer : WheelyVehicle
    {
        public int Volume { get; set; }
        public override string Name
        {
            get { return "Trailer"; }
        }
        public Trailer(int volume, int price, string manufacturer) : base(price, manufacturer)
        {
            Wheels = 4;
            Volume = volume;
        }

        public Trailer(Trailer origin) : base(origin.Price, origin.Manufacturer)
        {
            VehicleIdentificationNumber = origin.VehicleIdentificationNumber;
            Wheels = origin.Wheels;
            Volume = origin.Volume;
            Owner = origin.Owner;
        }

        public Trailer() : base(0, "")
        {
            Wheels = 0;
            Volume = 0;
            Owner = "";
        }

        public override string ToString()
        {
            return $"Trailer ({VehicleIdentificationNumber})";
        }
    }
}

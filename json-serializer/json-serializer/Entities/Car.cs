﻿namespace json_serializer.Entities
{
    public class Car : WheelyVehicle 
    {
        public int Seats { get; set; }
        public override string Name
        {
            get { return "Car"; }
        }
        public Car(int seats, int price, string manufacturer) : base(price, manufacturer)
        {
            Wheels = 4;
            Seats = seats;
        }

        public Car(Car origin) : base(origin.Price, origin.Manufacturer)
        {
            VehicleIdentificationNumber = origin.VehicleIdentificationNumber;
            Wheels = origin.Wheels;
            Seats = origin.Seats;
            Owner = origin.Owner;
        }

        public Car() : base(0, "(no manufacturer)")
        {
            Wheels = 0;
            Seats = 0;
            Owner = "(no owner)";
        }

        public override string ToString()
        {
            return $"Car ({VehicleIdentificationNumber})";
        }
    }
}

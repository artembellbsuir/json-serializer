﻿namespace json_serializer.Entities
{
    public class Truck : WheelyVehicle
    {
        public int CarryingCapacity { get; set; }
        public override string Name
        {
            get { return "Truck"; }
        }
        public Truck(int carryingCapacity, int price, string manufacturer) : base(price, manufacturer)
        {
            Wheels = 4;
            CarryingCapacity = carryingCapacity;
        }

        public Truck(Truck origin) : base(origin.Price, origin.Manufacturer)
        {
            VehicleIdentificationNumber = origin.VehicleIdentificationNumber;
            Wheels = origin.Wheels;
            CarryingCapacity = origin.CarryingCapacity;
            Owner = origin.Owner;
        }

        public Truck() : base(0, "(no manufacturer)")
        {
            Wheels = 0;
            CarryingCapacity = 0;
            Owner = "(no owner)";
        }

        public override string ToString()
        {
            return $"Truck ({VehicleIdentificationNumber})";
        }
    }
}

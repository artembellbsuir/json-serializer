﻿namespace json_serializer.Entities
{
    public class TractorUnit : WheelyVehicle
    {
        public int Gears { get; set; }
        public override string Name
        {
            get { return "Tractor Unit"; }
        }

        public TractorUnit(int gears, int price, string manufacturer) : base(price, manufacturer)
        {
            Gears = gears;
            Wheels = 2;
        }

        public TractorUnit(TractorUnit origin) : base(origin.Price, origin.Manufacturer)
        {
            VehicleIdentificationNumber = origin.VehicleIdentificationNumber;
            Wheels = origin.Wheels;
            Gears = origin.Gears;
            Owner = origin.Owner;
        }

        public TractorUnit() : base(0, "(no manufacturer)")
        {
            Wheels = 0;
            Gears = 0;
            Owner = "(no owner)";
        }

        public override string ToString()
        {
            return $"Tractor Unit ({VehicleIdentificationNumber})";
        }
    }
}

﻿namespace json_serializer.Entities
{
    public class Motorcycle : WheelyVehicle
    {
        public int Mileage { get; set; }
        public override string Name
        {
            get { return "Motorcycle"; }
        }

        public Motorcycle(int mileage, int price, string manufacturer) : base(price, manufacturer)
        {
            Mileage = mileage;
            Wheels = 2;
        }

        public Motorcycle(Motorcycle origin) : base(origin.Price, origin.Manufacturer)
        {
            VehicleIdentificationNumber = origin.VehicleIdentificationNumber;
            Wheels = origin.Wheels;
            Mileage = origin.Mileage;
            Owner = origin.Owner;
        }

        public Motorcycle() : base(0, "(no manufacturer)")
        {
            Wheels = 0;
            Mileage = 0;
            Owner = "(no owner)";
        }

        public override string ToString()
        {
            return $"Motorcycle ({VehicleIdentificationNumber})";
        }
    }
}

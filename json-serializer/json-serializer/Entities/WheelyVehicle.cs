﻿using System;

namespace json_serializer.Entities
{
    public abstract class WheelyVehicle
    {
        public string VehicleIdentificationNumber { get; set; }
        public string Manufacturer { get; set; }
        public int Price { get; set; }
        public string Owner { get; set; }
        public int Wheels { get; set; } = 0;

        public WheelyVehicle(int price, string manufacturer)
        {
            VehicleIdentificationNumber = Guid.NewGuid().ToString("N");
            Owner = "(no owner)";
            Price = price;
            Manufacturer = manufacturer;
        }
        public virtual string Name
        {
            get { return "Vehicle"; }
        }
    }
}

﻿using Newtonsoft.Json;
using System.IO;

namespace json_serializer.Serializer
{
    public class CustomJsonSerializer<T> : ISerializer<T>
    {
        public CustomJsonSerializer() { }

        public T Deserialize(string FileName)
        {
            T obj;
            obj = JsonConvert.DeserializeObject<T>(File.ReadAllText(FileName), new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
            return obj;
        }

        public void Serialize(T obj, string FileName)
        {
            using (StreamWriter file = File.CreateText(FileName))
            {
                JsonSerializer serializer = new JsonSerializer
                {
                    TypeNameHandling = TypeNameHandling.All,
                    Formatting = Formatting.Indented
                };
                serializer.Serialize(file, obj);
            }
        }
    }
}

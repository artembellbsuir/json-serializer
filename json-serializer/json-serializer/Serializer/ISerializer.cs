﻿namespace json_serializer.Serializer
{
    public interface ISerializer<T>
    {
        void Serialize(T obj, string FileName);
        T Deserialize(string FileName);
    }
}
